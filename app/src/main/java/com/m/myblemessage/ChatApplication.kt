package com.m.myblemessage

import android.app.Application
import android.os.StrictMode
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.NightMode
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.m.myblemessage.data.model.BluetoothConnector
import com.m.myblemessage.data.model.ProfileManager
import com.m.myblemessage.data.model.UserPreferences
import com.m.myblemessage.ui.util.ThemeHolder
import com.m.myblemessage.di.*
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.startKoin
import org.koin.core.scope.Scope

class ChatApplication : Application(), LifecycleObserver, ThemeHolder {

    var isConversationsOpened = false
    var currentChat: String? = null

    @NightMode
    private var nightMode: Int = AppCompatDelegate.MODE_NIGHT_NO

    private val connector: BluetoothConnector by inject()
    private val profileManager: ProfileManager by inject()
    private val preferences: UserPreferences by inject()

    private lateinit var localeSession: Scope

    override fun onCreate() {
        super.onCreate()

        startKoin(
            this, listOf(
                applicationModule,
                bluetoothConnectionModule, databaseModule, localStorageModule, viewModule
            )
        )
        localeSession = getKoin().createScope(localeScope)

        nightMode = preferences.getNightMode()

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

        if (BuildConfig.DEBUG) {

            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    internal fun prepareConnection() {
        if (!profileManager.getUserName().isEmpty()) {
            connector.prepare()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    internal fun releaseConnection() {
        connector.release()
    }

    override fun setNightMode(@NightMode nightMode: Int) {
        this.nightMode = nightMode
    }

    override fun getNightMode() = nightMode
}
