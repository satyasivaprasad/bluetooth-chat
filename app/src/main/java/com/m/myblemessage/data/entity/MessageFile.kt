package com.m.myblemessage.data.entity

data class MessageFile(
    val uid: Long,
    val filePath: String?,
    val own: Boolean
)
