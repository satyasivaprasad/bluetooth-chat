package com.m.myblemessage.data.model

import android.net.Uri

interface FileManager {
    suspend fun extractApkFile(): Uri?
}
