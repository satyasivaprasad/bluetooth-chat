package com.m.myblemessage.data.model

interface OnPrepareListener {
    fun onPrepared()
    fun onError()
}