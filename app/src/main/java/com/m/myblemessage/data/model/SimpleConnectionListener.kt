package com.m.myblemessage.data.model

import android.bluetooth.BluetoothDevice
import com.m.myblemessage.data.entity.Conversation
import com.m.myblemessage.data.model.OnConnectionListener

abstract class SimpleConnectionListener : OnConnectionListener {

    override fun onConnecting() {}

    abstract override fun onConnected(device: BluetoothDevice)

    override fun onConnectedIn(conversation: Conversation) {}

    override fun onConnectedOut(conversation: Conversation) {}

    abstract override fun onConnectionLost()

    abstract override fun onConnectionFailed()

    override fun onConnectionDestroyed() {}

    override fun onDisconnected() {}

    override fun onConnectionAccepted() {}

    override fun onConnectionRejected() {}

    override fun onConnectionWithdrawn() {}
}
