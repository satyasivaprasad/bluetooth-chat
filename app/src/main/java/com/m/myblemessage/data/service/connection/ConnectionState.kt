package com.m.myblemessage.data.service.connection

enum class ConnectionState {
    CONNECTED, CONNECTING, NOT_CONNECTED, REJECTED, PENDING, LISTENING
}
