package com.m.myblemessage.data.service.connection

enum class ConnectionType {
    INCOMING, OUTCOMING
}
