package com.m.myblemessage.ui.view

import com.m.myblemessage.data.entity.MessageFile

interface ReceivedImagesView {
    fun displayImages(imageMessages: List<MessageFile>)
    fun showNoImages()
}
