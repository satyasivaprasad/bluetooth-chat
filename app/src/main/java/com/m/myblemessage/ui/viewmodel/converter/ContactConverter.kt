package com.m.myblemessage.ui.viewmodel.converter

import com.amulyakhare.textdrawable.TextDrawable
import com.m.myblemessage.data.entity.Conversation
import com.m.myblemessage.ui.viewmodel.ContactViewModel
import com.m.myblemessage.utils.getFirstLetter

class ContactConverter {

    fun transform(conversation: Conversation): ContactViewModel {
        return ContactViewModel(
                conversation.deviceAddress,
                "${conversation.displayName} (${conversation.deviceName})",
                TextDrawable.builder()
                        .buildRound(conversation.displayName.getFirstLetter(), conversation.color)
        )
    }

    fun transform(conversationCollection: Collection<Conversation>): List<ContactViewModel> {
        return conversationCollection.map {
            transform(it)
        }
    }
}
