package com.m.myblemessage.utils

data class Size(val width: Int, val height: Int)
